# tu-lAma

** tu-lAma let Alma be more adroit **

## General function

*   Expand list of search items (IDs, MMS-IDs, ISBN): with **Ctrl-Alt-Y**
*   Syntax highlighting for Marc21 with different color profiles on Record View too
*   High contrast (black font), better visibility of marked search terms
*   Warnings on duplicates or records marked for deletion (970) or other rules. On levels &lt;=2 only the matching fields will be marked
*   Dynamic loading of bookmarklets (macros)
*   Sorting of "Item List" by description **Ctrl-Alt-D**
*   Move messages to bottom of window
*   Process slip
*   Warning on wrong location by IP address

## MD-Editor

*   Diff-Viewer **Ctrl-Alt-Y**: Compare draft in editor with saved version and show changes.
Compare current record with selected history version, or compare two different records in split screen.
*   Jump to first empty field or scroll to end of record
*   Filter templates with configurable value (Ctrl-Alt-T)
    Press __Backspace__ to apply filter, select entry with __Cursor-Down__ and __Enter__, use Tab to activate "Ok" and finish with __Enter__.
Or use your mouse.
*   Replace empty 852_8_c on save
*   Warning on wrong cataloging level
*   Mark templates and rules with different header color
*   GND autocomplete for theses contributors
*   Disable LC tabs to make GND tab visible

## Item Editor

*   Apply template Ctrl-Alt-T, change template settings Shift-Ctrl-Alt-T
*   Change item policy Ctrl-Alt-C, change value Shift-Ctrl-Alt-C
*   Change proces-type Ctrl-Alt-B, change value Shift-Ctrl-Alt-B
